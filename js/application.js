window.Calendar = Ember.Application.create();

//from http://stackoverflow.com/a/11597805
Ember.SelectOption.reopen({
    classNameBindings: 'optionClass',
    optionClass: function(){
        var classPath = this.get('parentView.optionClassPath');
        return this.get(classPath);
    }.property('parentView.optionClassPath')
});