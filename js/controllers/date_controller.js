Calendar.DateController = Ember.ObjectController.extend({
  actions: {
    goToSelectedYear: function(value) {
      if (value < 1970) {value = 1970}
      if (value > 2200) {value = 2200}
      this.transitionToRoute('date', {year: value, month: this.get('model.month'), day: this.get("model.day")});
    },

    gotToSelectedDay: function(value) {
      this.transitionToRoute('date',{year: this.get('model.year'), month: this.get('model.month'), day: value})
    },

    goToToday: function(){
      this.transitionToRoute('date',{year: this.get('currentYear'), month: this.get('curMonth'), day: this.get('currentDay')})
    }
  },

  months: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],

  selectedMonth: null,
  
  dateTable: function(){
    //функция вычисления номера недели
    Date.prototype.getWeek = function () {  
        // Create a copy of this date object  
        var target  = new Date(this.valueOf());  
        // ISO week date weeks start on monday; so correct the day number  
        var dayNr   = (this.getDay() + 6) % 7;  
        // ISO 8601 states that week 1 is the week with the first thursday of that year. Set the target date to the thursday in the target week  
        target.setDate(target.getDate() - dayNr + 3);  
        // Store the millisecond value of the target date  
        var firstThursday = target.valueOf();  
        // Set the target to the first thursday of the year. First set the target to january first  
        target.setMonth(0, 1);  
        // Not a thursday? Correct the date to the next thursday  
        if (target.getDay() != 4) {  
            target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);  
        }
        // The weeknumber is the number of weeks between the first thursday of the year and the thursday in the target week  
        return 1 + Math.ceil((firstThursday - target) / 604800000); // 604800000 = 7 * 24 * 3600 * 1000  
    }

    var weekArr = [];
    var week = new Date().getWeek();
    
    var D1 = new Date();
    dayLast = new Date(this.get('model.year'), this.get('model.month'), 0).getDate(); // последний день месяца
    dayNLast = new Date(this.get('model.year'), this.get('model.month')-1, dayLast).getDay(); // день недели последнего дня месяца
    dayNFirst = new Date(this.get('model.year'), this.get('model.month')-1, 1).getDay(); // день недели первого дня месяца

    var dd = 0;
    for ( var j = 0; j<7; ++j){
      var dayArr = [];
      var abc = new Date(this.get('model.year'), this.get('model.month')-1, dd+1).getWeek();
      dayArr.push({name: abc, weeknumber: true ,class: "week"});
      for(var  i = 0; i < 7; i++) {
        // пустые клетки до первого дня текущего месяца
        if (dayNFirst != 0 && dd == 0) {
          for(var  ab = 1; ab < dayNFirst; ab++) dayArr.push({name: ""});
        }else if ( dd == 0){ // если первый день месяца выпадает на воскресенье, то требуется 7 пустых клеток 
          for(var  ab = 0; ab < 6; ab++) dayArr.push({name: ""});
        }

        // дни месяца
        dd += 1;
        
        if (new Date(this.get('model.year'), this.get('model.month')-1, dd).getDay() == 0){
          if (dd <= dayLast){
            if (this.get('model.day') == dd){
              dayArr.push({name: dd, class: "sanday selectedDay"});
            }else{
              dayArr.push({name: dd, class: "sanday"});
            }
          }
          break;
        }else{
          if (dd <= dayLast){
            if ( (dd == D1.getDate()) && (this.get('model.month')-1 == D1.getMonth()) && (this.get('model.year') == D1.getFullYear()) ) {
              if (this.get('model.day') == dd){
                dayArr.push({name: dd, class: "selectedDay today"});
              }else{
                dayArr.push({name: dd, class: "today"});
              }
            }else{
              if (this.get('model.day') == dd){
                dayArr.push({name: dd, class: "selectedDay"});
              }else{
                dayArr.push({name: dd});
              }
            }
            // пустые клетки после последнего дня месяца
            if (dayNLast != 0 && dd == dayLast) {
              for(var  z = dayNLast; z < 7; z++) {
                dayArr.push({name: ""});
              };
            }
          }
        }
      }
      weekArr.push(dayArr)
      if( dd >= dayLast) break;
    }
    return weekArr;
  }.property('model.year','model.month','model.day'),
  
  currentYear: function() {
    return new Date().getFullYear();
  }.property('date'),

  currentMonth: function(){
    return this.months[new Date().getMonth()];
  }.property('date'),

  currentDay: function(){
    return new Date().getDate();
  }.property('date'),

  curMonth: function() {
    return new Date().getMonth()+1
  }.property('model.month'),

  monthCombo: function() {
    var monthsObjArr = []
    for (var i = 1; i <= this.months.length; i++){
      if( i == new Date().getMonth() + 1){
        monthsObjArr.push({title: this.months[i - 1], class: 'currentMonth', isSelected:true, nom: i});
      } else {
        monthsObjArr.push({title: this.months[i - 1], isSelected: false, nom: i});
      };
    };
    return monthsObjArr;
  }.property('date'),

  selectMonth: function() {
    if (this.get('selectedMonth').nom != this.get('model.month')){
      this.transitionToRoute('date', {year: this.get('model.year'), month: this.get('selectedMonth').nom, day: this.get('model.day')})
    }
  }.observes('selectedMonth'),

  changeMonth: function () {
    if ( !Em.isEmpty(this.get('model.month'))){
      var month = this.get('monthCombo').findBy('nom',parseInt(this.get('model.month')));
      console.log(month);
      this.set('selectedMonth', month);
    }
  }.observes('model.month')
});