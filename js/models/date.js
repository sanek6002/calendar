Calendar.DateModel = DS.Model.extend({
	day:   DS.attr('int'),
	month: DS.attr('int'),
	year:  DS.attr('int'),
}); 