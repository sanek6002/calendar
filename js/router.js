Calendar.Router.map(function(){
	this.resource('date', {path: '/year/:year/month/:month/day/:day'})
});

Calendar.IndexRoute = Ember.Route.extend({
	model: function() {
		return {
			year: new Date().getFullYear(),
			month: new Date().getMonth()+1,
			day: new Date().getDate()
		};
	},

	afterModel: function (model) {
		this.transitionTo('date', model);
	}
});
Calendar.DateRoute = Ember.Route.extend({
	model: function(params) {
		return {
			year: params.year,
			month: params.month,
			day: params.day
		};
	}

})